// Fill out your copyright notice in the Description page of Project Settings.
#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include <Engine/StaticMeshActor.h>
#include "TPS/Character/TPSInventoryComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetReplicates(true);

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	
	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority())
	{
		FireTick(DeltaTime);
		ReloadTick(DeltaTime);
		DispersionTick(DeltaTime);

		MagazineDropTick(DeltaTime);
		ShellDropTick(DeltaTime);
	}
}

void AWeaponDefault::FireTick(float Deltatime)
{
	if (WeaponFiring && GetWeaponAmmo() > 0 && !WeaponReloading)
	{
		if (FireTimer < 0.0f)
		{
			Fire();
		}
		else
		{
			FireTimer -= Deltatime;
		}
	}
}

void AWeaponDefault::ReloadTick(float Deltatime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= Deltatime;
		}
	}
}

void AWeaponDefault::DispersionTick(float Deltatime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if (ShowDebug)
	{
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
	}
}

void AWeaponDefault::MagazineDropTick(float Deltatime)
{
	if (CanDropMagazine)
	{
		if (MagazineDropTimer <= 0.0f)
		{
			CanDropMagazine = false;
			MagazineDrop_OnServer(WeaponSettings.MagazineDrop);
		}
		else
		{
			MagazineDropTimer -= Deltatime;
		}
	}
}

void AWeaponDefault::ShellDropTick(float Deltatime)
{
	if (CanDropShell)
	{
		if (ShellDropTimer <= 0.0f)
		{
			CanDropShell = false;
			ShellDrop_OnServer(WeaponSettings.ShellBullets);
		}
		else
		{
			ShellDropTimer -= Deltatime;
		}
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh) // SkeletalMeshWeapon != nullptr and(&&) SkeletalMeshWeapon->SkeletalMesh == nullptr or 0 
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}

	UpdateStateWeapon_OnServer(EMovementState::Run_state);
}

void AWeaponDefault::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	if (CheckWeaponCanFire())
	{
		WeaponFiring = bIsFire;
	}
	else
	{
		WeaponFiring = false;
	}
	//FireTimer = 0.01f;
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSettings.ProjectileSettings;
}

void AWeaponDefault::Fire()
{
	//on serv by bool weaponfire

	UAnimMontage* AnimToFire;
	if (WeaponAiming)
	{
		AnimToFire = WeaponSettings.AnimCharFireIronsights;
	}
	else
	{
		AnimToFire = WeaponSettings.AnimCharFire;
	}

	if (WeaponSettings.ShellBullets)
	{
		CanDropShell = true;
		ShellDropTimer = 0.0f;
	}
	FireTimer = WeaponSettings.RateOfFire;
	AdditionalWeaponInfo.Ammo = AdditionalWeaponInfo.Ammo - 1;
	ChangeDispersionByShot();

	OnWeaponFireStart.Broadcast(AnimToFire);

	FxWeaponFire_Multicast(WeaponSettings.EffectFireWeapon, WeaponSettings.SoundFireWeapon);
	
	int8 NumberProjectile = GetNumberProjectileByShot();


	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++) // shotgun
		{
			EndLocation = GetFireEndLocation();

			FVector Dir = EndLocation - SpawnLocation;

			Dir.Normalize();

			FMatrix myMatrix(Dir, FVector(0, 0, 0), FVector(0, 0, 0), FVector::ZeroVector);
			SpawnRotation = myMatrix.Rotator();

			if (ProjectileInfo.Projectile)
			{

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile;
				myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSettings.ProjectileSettings);
				}
			}
			else
			{

				FVector ForwardVector = ShootLocation->GetForwardVector();
				FHitResult HitResult;

				TArray<AActor*> ActorsToIgnore;
				ActorsToIgnore.Add(this);
				ActorsToIgnore.Add(GetOwner());

				UKismetSystemLibrary::LineTraceSingle(
					GetWorld(),
					SpawnLocation,
					EndLocation * WeaponSettings.DistanceTrace,
					UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel2),
					false,
					ActorsToIgnore,
					EDrawDebugTrace::None,
					HitResult,
					true
				);

				if (HitResult.GetActor() && HitResult.PhysMaterial.IsValid())
				{
					EPhysicalSurface mySurfaceType = UGameplayStatics::GetSurfaceType(HitResult);

					if (ProjectileInfo.HitDecals.Contains(mySurfaceType))
					{
						UMaterialInterface* myMaterial = ProjectileInfo.HitDecals[mySurfaceType];

						if (myMaterial && HitResult.Component.IsValid())
						{
							TraceHitDecal_Multicast(myMaterial, HitResult);
						}
					}
					if (ProjectileInfo.HitFXs.Contains(mySurfaceType))
					{
						UParticleSystem* myParticle = ProjectileInfo.HitFXs[mySurfaceType];
						if (myParticle)
						{
							TraceHitFX_Multicast(myParticle, HitResult);
						}
					}

					if (ProjectileInfo.HitSound)
					{
						TraceHitSound_Multicast(ProjectileInfo.HitSound, HitResult);
					}

					UTypes::AddEffectBySurfaceType(HitResult.GetActor(), HitResult.BoneName, ProjectileInfo.Effect, mySurfaceType);

					UGameplayStatics::ApplyPointDamage(HitResult.GetActor(), ProjectileInfo.ProjectileDamage, HitResult.TraceStart, HitResult, GetInstigatorController(), this, NULL);
				}
			}
		}
	}

	if (GetWeaponAmmo() <= 0 && !WeaponReloading)
	{
		if (CheckCanWeaponReload())
		{
			InitReload();
		}
	}
}

void AWeaponDefault::UpdateStateWeapon_OnServer_Implementation(EMovementState NewMovementState)
{
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		WeaponAiming = true;
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::AimWalk_State:
		WeaponAiming = true;
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.AimWalk_StateDispersioReduction;
		break;
	case EMovementState::Walk_state:
		WeaponAiming = false;
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.Walk_StateDisperionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Walk_StateDispersionReduction;
		break;
	case EMovementState::Run_state:
		WeaponAiming = false;
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Run_StateDispersionReduction;
		break;
	case EMovementState::Sprint_State:
		WeaponAiming = false;
		BlockFire = true;
		SetWeaponStateFire_OnServer(false);//set trigger to false
		//Block fire
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.0f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	//UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSettings.DistanceTrace, GetCurrentDispersion() * PI / 180.0f, GetCurrentDispersion() * PI / 180.0f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		}
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSettings.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		}	
	}

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}

	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSettings.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponAmmo()
{
	return AdditionalWeaponInfo.Ammo;
}

void AWeaponDefault::InitReload()
{
	//server
	WeaponReloading = true;

	ReloadTimer = WeaponSettings.ReloadTime;

	if (WeaponSettings.SoundReloadWeapon)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSettings.SoundReloadWeapon, ShootLocation->GetComponentLocation());
	}

	if (WeaponSettings.AnimCharReload)
	{
		OnWeaponReloadStart.Broadcast(WeaponSettings.AnimCharReload);
	}
	if (WeaponSettings.MagazineDrop)
	{
		CanDropMagazine = true;
		MagazineDropTimer = 0.8f;
	}
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	int8 AviableAmmoFromInventory = GetAvaibleAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	int8 NeedToReload = WeaponSettings.MaxAmmo - AdditionalWeaponInfo.Ammo;

	if (NeedToReload > AviableAmmoFromInventory)
	{
		AdditionalWeaponInfo.Ammo += AviableAmmoFromInventory;
		AmmoNeedTakeFromInv = AviableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.Ammo += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}

	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;

	OnWeaponReloadEnd.Broadcast(false, 0);
	CanDropMagazine = false;
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UTPSInventoryComponent* MyInv = Cast<UTPSInventoryComponent>(GetOwner()->GetComponentByClass(UTPSInventoryComponent::StaticClass()));
		if (MyInv)
		{
			int8 AvaibleAmmoForWeapon;
			if (!MyInv->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvaibleAmmoForWeapon))
			{
				result = false;
				MyInv->OnWeaponNotHaveRound.Broadcast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
			}
			else
			{
				MyInv->OnWeaponHaveRound.Broadcast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
			}
		}
	}

	return result;
}

int8 AWeaponDefault::GetAvaibleAmmoForReload()
{
	int8 AvaibleAmmoForWeapon = WeaponSettings.MaxAmmo;
	if (GetOwner())
	{
		UTPSInventoryComponent* MyInv = Cast<UTPSInventoryComponent>(GetOwner()->GetComponentByClass(UTPSInventoryComponent::StaticClass()));
		if (MyInv)
		{
			if (MyInv->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvaibleAmmoForWeapon))
			{
				//AvaibleAmmoForWeapon = AvaibleAmmoForWeapon;
			}
		}
	}

	return AvaibleAmmoForWeapon;
}

void AWeaponDefault::FxWeaponFire_Multicast_Implementation(UParticleSystem* FxFire, USoundBase* SoundFire)
{
	if (FxFire)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxFire, ShootLocation->GetComponentTransform());
	}
	if (SoundFire)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SoundFire, ShootLocation->GetComponentLocation());
	}
}

void AWeaponDefault::MagazineDrop_OnServer_Implementation(UStaticMesh* DropMagazineMesh)
{
	if (WeaponSettings.MagazineDrop)
	{
		MagazineDrop_Multicast(DropMagazineMesh);
	}
}

void AWeaponDefault::MagazineDrop_Multicast_Implementation(UStaticMesh* DropMagazineMesh)
{
	FVector SpawnLocation = GetActorLocation();

	AStaticMeshActor* DroppedMagazine = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), SpawnLocation, FRotator::ZeroRotator);
	if (DroppedMagazine)
	{
		UStaticMeshComponent* MeshComponent = DroppedMagazine->GetStaticMeshComponent();
		MeshComponent->Mobility = EComponentMobility::Movable;
		MeshComponent->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		MeshComponent->SetSimulatePhysics(true);
		MeshComponent->SetStaticMesh(DropMagazineMesh);

		DroppedMagazine->SetLifeSpan(20.0f);
		DroppedMagazine->SetActorTickEnabled(false);

		MeshComponent->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	}
}

void AWeaponDefault::ShellDrop_OnServer_Implementation(UStaticMesh* DropShellMesh)
{
	if (WeaponSettings.ShellBullets)
	{
		ShellDrop_Multicast(DropShellMesh);
	}
}

void AWeaponDefault::ShellDrop_Multicast_Implementation(UStaticMesh* DropShellMesh)
{
	FVector SpawnLocation = SceneComponent->GetComponentLocation();
	FRotator SpawnRotation = ShootLocation->GetComponentRotation();

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();

	AStaticMeshActor* DroppedShell = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), SpawnLocation, SpawnRotation, SpawnParams);
	if (DroppedShell)
	{
		UStaticMeshComponent* MeshComponent = DroppedShell->GetStaticMeshComponent();
		MeshComponent->Mobility = EComponentMobility::Movable;
		MeshComponent->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));

		MeshComponent->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Ignore);
		MeshComponent->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);

		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		MeshComponent->SetSimulatePhysics(true);
		MeshComponent->SetStaticMesh(DropShellMesh);

		MeshComponent->SetWorldScale3D(FVector(4.0f, 4.0f, 4.0f));

		DroppedShell->SetLifeSpan(10.0f);
		DroppedShell->SetActorTickEnabled(false);

		//FVector ImpulseDirection = FVector(0.0f, 0.0f, -5.0f);
		FVector ImpulseDirection = FVector(FMath::RandRange(-3.0f, 3.0f), FMath::RandRange(-3.0f, 3.0f), 5.0f);
		MeshComponent->AddImpulse(ImpulseDirection);
	}
}

void AWeaponDefault::UpdateWeaponByCharMovementState_OnServer_Implementation(FVector NewShootEndLocation, bool NewShouldReduceDispersion)
{
	ShootEndLocation = NewShootEndLocation;
	ShouldReduceDispersion = NewShouldReduceDispersion;
}

void AWeaponDefault::TraceHitDecal_Multicast_Implementation(UMaterialInterface* Material, FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(Material, FVector(20.0f), HitResult.Component.Get(), NAME_None, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
}

void AWeaponDefault::TraceHitFX_Multicast_Implementation(UParticleSystem* Particle, FHitResult HitResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Particle, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(1.0f)));
}

void AWeaponDefault::TraceHitSound_Multicast_Implementation(USoundBase* Sound, FHitResult HitResult)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, HitResult.ImpactPoint);
}


void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeaponDefault, AdditionalWeaponInfo);
	DOREPLIFETIME(AWeaponDefault, ShootEndLocation);
	DOREPLIFETIME(AWeaponDefault, WeaponReloading);

}
