// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Weapon/ProjectileDefault_Grenade.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority())
	{
		TimerExplose(DeltaTime);
	}
}

//counts down time until grenade explose
void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			Explose_OnServer();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//timer diabled - explose immediately
	if (!TimerEnabled)
	{
		Explose_OnServer();
	}
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//init grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose_OnServer_Implementation()
{
	//debug spheres
	if (ShowDebug)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ExploseMaxRadiusDamage, 10, FColor::Cyan, false, 5.0f, 0, 2.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ExploseMaxRadiusDamage + ProjectileSettings.ExploseFalloffRadius, 10, FColor::Emerald, false, 5.0f, 0, 2.0f);
	}
	//

	TimerEnabled = false;

	//fxs
	if (ProjectileSettings.ExploseFX)
	{
		GrenadeExploseFx_Multicast(ProjectileSettings.ExploseFX);
	}
	if (ProjectileSettings.ExploseSound)
	{
		GrenadeExploseSound_Multicast(ProjectileSettings.ExploseSound);
	}
	//


	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff
	(
		GetWorld(),
		ProjectileSettings.ExploseMaxDamage,
		ProjectileSettings.ExploseMaxDamage * ProjectileSettings.ExploseFalloffCoef,
		GetActorLocation(),
		ProjectileSettings.ExploseMaxRadiusDamage,
		ProjectileSettings.ExploseMaxRadiusDamage + ProjectileSettings.ExploseFalloffRadius,
		5,//min dmg
		NULL, //type dmg
		IgnoredActor, //ignored actors
		nullptr, //comtroller
		nullptr //actor who deals dmg
	);

	this->Destroy();

}

void AProjectileDefault_Grenade::GrenadeExploseFx_Multicast_Implementation(UParticleSystem* Particle)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Particle, GetActorLocation(), GetActorRotation(), FVector(1.0f));
}

void AProjectileDefault_Grenade::GrenadeExploseSound_Multicast_Implementation(USoundBase* Sound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, GetActorLocation());
}
