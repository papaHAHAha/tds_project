// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Weapon/ProjectileDefault.h"
#include "WeaponDefault.generated.h"


//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponFireStart);//ToDo Delegate on event weapon fire - Anim char, state char...
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, Anim);

UCLASS()
class TPS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();
	
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponFireStart OnWeaponFireStart;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY()
	FName IdWeaponName;

	UPROPERTY()
	FWeaponInfo WeaponSettings;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponInfo AdditionalWeaponInfo;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireTick(float Deltatime);
	void ReloadTick(float Deltatime);
	void DispersionTick(float Deltatime);
	void MagazineDropTick(float Deltatime);
	void ShellDropTick(float Deltatime);

	void WeaponInit();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponAiming = false;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	bool WeaponReloading = false;

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetWeaponStateFire_OnServer(bool bIsFire);

	bool CheckWeaponCanFire();

	FProjectileInfo GetProjectile();

	void Fire();

	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState NewMovementState);

	void ChangeDispersionByShot();

	float GetCurrentDispersion() const;

	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	FVector GetFireEndLocation() const;

	int8 GetNumberProjectileByShot() const;

	//timers
	float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic debug") // debug on remove
	float ReloadTime = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropLogic")
	float MagazineDropTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropLogic")
	float ShellDropTimer = 0.0f;

	//flags
	bool BlockFire = false;
	bool CanDropMagazine = false;
	bool CanDropShell = false;
	//Dispersion
	bool ShouldReduceDispersion = false;

	float CurrentDispersion = 0.0f;

	float CurrentDispersionMax = 1.0f;

	float CurrentDispersionMin = 0.1f;

	float CurrentDispersionRecoil = 0.1f;

	float CurrentDispersionReduction = 0.1f;

	UPROPERTY(Replicated)
	FVector ShootEndLocation = FVector(0);

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponAmmo();

	void InitReload();

	void FinishReload();

	void CancelReload();

	bool CheckCanWeaponReload();
	int8 GetAvaibleAmmoForReload();

	UFUNCTION(Server, Reliable)
	void MagazineDrop_OnServer(UStaticMesh* DropMagazineMesh);

	UFUNCTION(Server, Reliable)
	void ShellDrop_OnServer(UStaticMesh* DropShellMesh);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "debug")
	bool ShowDebug = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "debug")
	float SizeVectorToChangeShootDirectionLogic = 100.0f;

	//
	UFUNCTION(Server, Unreliable)
	void UpdateWeaponByCharMovementState_OnServer(FVector NewShootEndLocation, bool NewShouldReduceDispersion);
	
	UFUNCTION(NetMulticast, Reliable)
	void ShellDrop_Multicast(UStaticMesh* DropShellMesh);

	UFUNCTION(NetMulticast, Reliable)
	void MagazineDrop_Multicast(UStaticMesh* DropMagazineMesh);

	UFUNCTION(NetMulticast, Unreliable)
	void FxWeaponFire_Multicast(UParticleSystem* FxFire, USoundBase* SoundFire);

	UFUNCTION(NetMulticast, Reliable)
	void TraceHitDecal_Multicast(UMaterialInterface* Material, FHitResult HitResult);

	UFUNCTION(NetMulticast, Reliable)
	void TraceHitFX_Multicast(UParticleSystem* Particle, FHitResult HitResult);

	UFUNCTION(NetMulticast, Reliable)
	void TraceHitSound_Multicast(USoundBase* Sound, FHitResult HitResult);
};
