// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPS/Weapon/ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void ImpactProjectile() override;

	UFUNCTION(Server, Reliable)
	void Explose_OnServer();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	bool TimerEnabled = false;

	float TimerToExplose = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	float TimeToExplose = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "debug")
	bool ShowDebug = false;

	UFUNCTION(NetMulticast, Reliable)
	void GrenadeExploseFx_Multicast(UParticleSystem* Particle);

	UFUNCTION(NetMulticast, Reliable)
	void GrenadeExploseSound_Multicast(USoundBase* Sound);
};
