// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPS/StateEffects/TPS_StateEffect.h"

#include "TPS_EnvironmentStructure.generated.h"

UCLASS()
class TPS_API ATPS_EnvironmentStructure : public AActor, public ITPS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATPS_EnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void RemoveEffect(UTPS_StateEffect* RemoveEffect);
	void RemoveEffect_Implementation(UTPS_StateEffect* RemoveEffect) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void AddEffect(UTPS_StateEffect* newEffect);
	void AddEffect_Implementation(UTPS_StateEffect* newEffect) override;


	//effect
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Settings")
	TArray<UTPS_StateEffect*> Effects;

	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UTPS_StateEffect* EffectAdd = nullptr;

	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTPS_StateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	FVector OffsetEffect = FVector(0);

	UFUNCTION()
	void EffectAdd_OnRep();

	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);

	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);

	UFUNCTION()
	void SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd);
};
