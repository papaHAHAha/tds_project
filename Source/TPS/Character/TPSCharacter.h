// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Weapon/WeaponDefault.h"
#include "TPS/Character/TPSInventoryComponent.h"
#include "TPS/Character/TPSCharacterHealthComponent.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPS/StateEffects/TPS_StateEffect.h"

#include "TPSCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeRollCooldown, bool, RollCooldown);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeCurrentStamina, float, CurrentStamina);

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter, public ITPS_IGameActor
{
	GENERATED_BODY()
protected:

	bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void BeginPlay() override;

	//inputs
	UFUNCTION(Server, Reliable)
	void StartRoll_OnServer();

	UFUNCTION()
	void InputAxisX(float Value);

	UFUNCTION()
	void InputAxisY(float Value);

	UFUNCTION()
	void InputAttackPressed();

	UFUNCTION()
	void InputAttackReleased();

	UFUNCTION()
	void InputWalkPressed();

	UFUNCTION()
	void InputWalkReleased();

	UFUNCTION()
	void InputAimPressed();

	UFUNCTION()
	void InputAimReleased();

	void TrySwitchNextWeapon();
	void TrySwitchPreviousWeapon();

	void OnStartSprint();
	void OnStopSprint();

	void TryAbilityEnabled();

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput_OnServer(Id);
	}

	//input flag
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UPROPERTY(Replicated)
	float CurrentStamina;
	
	UPROPERTY(Replicated)
	int32 CurrentIndexWeapon = 0;

	bool WalkEnabled = false;
	bool SprintEnabled = false;


	UPROPERTY(Replicated)
	bool bIsRollCoolDown;

	UPROPERTY(Replicated)
	bool bIsRolling = false;

	UPROPERTY(Replicated)
	FVector RollStartLocation;

	UPROPERTY(Replicated)
	FVector RollEndLocation;

	UPROPERTY(Replicated)
	EMovementState MovementState = EMovementState::Run_state;

	UPROPERTY(Replicated)
	AWeaponDefault* CurrentWeapon = nullptr;

	UDecalComponent* CurrentCursor = nullptr;

	UPROPERTY(Replicated)
	TArray<UTPS_StateEffect*> Effects;

	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UTPS_StateEffect* EffectAdd = nullptr;

	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTPS_StateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;

	FName LastHitBoneName;

	UFUNCTION()
	void CharDead();

	UFUNCTION(NetMulticast, Reliable)
	void EnableRagdoll_Multicast();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

public:
	ATPSCharacter();

	UPROPERTY(BlueprintAssignable, Category = "Movement")
	FOnChangeRollCooldown OnChangeRollCooldown;

	UPROPERTY(BlueprintAssignable, Category = "Movement")
	FOnChangeCurrentStamina OnChangeCurrentStamina;

	FTimerHandle TimerHandle_RagDollTimer;

	FTimerHandle StaminaTimerHandle;

	FTimerHandle SprintTimerHandle;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* InputComp) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UTPSInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	class UTPSCharacterHealthComponent* CharHealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20.f, 40.f, 40.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool AimEnabled = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	TArray<UAnimMontage*> DeadsAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	UAnimMontage* RollAnim = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TSubclassOf<UTPS_StateEffect> AbilityEffect;
	
private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:

	//tick func
	UFUNCTION()
	void MovementTick(float DeltaTime);

	//func
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	UFUNCTION(BlueprintCallable)
	void UpdateSprint();

	UFUNCTION(BlueprintCallable)
	void UpdateStamina();

	UFUNCTION(NetMulticast, Reliable)
	void ChangeCurrentStaminaEvent_Multicast(float Stamina);

	UFUNCTION(NetMulticast, Reliable)
	void ChangeRollCooldownEvent_Multicast(bool RollCooldown);

	UFUNCTION(Server, Reliable)
	void UpdateRoll_OnServer();

	UFUNCTION(NetMulticast, Reliable)
	void UpdateRoll_Multicast(FVector NewLocation);

	UFUNCTION(Server, Reliable)
	void EndRoll_OnServer();

	UFUNCTION(BlueprintCallable)
	void ResetRollCoolDown();

	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);

	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();

	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);

	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);

	UFUNCTION()
	void WeaponFireStart(UAnimMontage* Anim);

	UFUNCTION(Server, Reliable)
	void TrySwitchWeaponToIndexByKeyInput_OnServer(int32 ToIndex);

	void DropCurrentWeapon();

	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);

	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	AWeaponDefault* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	UDecalComponent* GetCursorToWorld();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	EMovementState GetMovementState();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<UTPS_StateEffect*> GetCurrentEffectsOnChar();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetCurrentWeaponIndex();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetIsAlive();

	//interface
	EPhysicalSurface GetSurfaceType() override;

	TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void RemoveEffect(UTPS_StateEffect* RemoveEffect);
	void RemoveEffect_Implementation(UTPS_StateEffect* RemoveEffect) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void AddEffect(UTPS_StateEffect* newEffect);
	void AddEffect_Implementation(UTPS_StateEffect* newEffect) override;

	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

	UFUNCTION(Server, Unreliable)
	void SetActorRotationYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
	void SetMovementState_Multicast(EMovementState NewState);

	UFUNCTION(Server, Reliable)
	void TryReloadWeapon_OnServer();

	UFUNCTION(NetMulticast, Reliable)
	void PlayAnim_Multicast(UAnimMontage* Anim);

	UFUNCTION()
	void EffectAdd_OnRep();

	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);

	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);

	UFUNCTION()
	void SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd);

private:

	bool bIsShiftPressed = false;
	bool bIsSprintAngleOk = false;
	float MaxSprintAngle = 0.0f;

	float RollAnimLength = 0.0f;
	float RollDistance = 0.0f;
	float RollInterpAlpha = 0.0f;
	float RollSpeed = 0.0f;
	float RollCoolDownTime = 0.0f;
	
	FTimerHandle RollTimerHandle;
	FTimerHandle RollUpdateTimerHandle;
	FTimerHandle RollCoolDownTimerHandle;
	

	float MaxStamina;
	float StaminaIncreaseRate = 5.0f;
	float StaminaDecreaseRate = 10.0f;
	float RunStaminaIncreaseRate = 1.0f;

};

