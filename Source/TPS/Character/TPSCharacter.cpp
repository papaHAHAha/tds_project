// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TPS/Game/TPSGameInstance.h"
#include "TPS/Weapon/ProjectileDefault.h"
#include "TPS/TPS.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"

ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	GetCharacterMovement()->NetworkSmoothingMode = ENetworkSmoothingMode::Linear;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UTPSInventoryComponent>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UTPSCharacterHealthComponent>(TEXT("HealthComponent"));

	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &ATPSCharacter::CharDead);
	}
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATPSCharacter::InitWeapon);
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;


	MaxStamina = 100.0f;
	CurrentStamina = 100.0f;

	MaxSprintAngle = 60.0f;

	bIsRolling = false;
	bIsRollCoolDown = false;
	RollCoolDownTime = 5.0f;
	RollSpeed = 600.f;

	//net
	bReplicates = true;
}

void ATPSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());

		if (myPC && myPC->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);

	CharacterUpdate();
}

void ATPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	//��������� ������������ � ������ ������ ������ � tick, ����� �� �������� ������ ��� ChangeMovementState � �� ��������� ��������� ���������(network)
	GetWorldTimerManager().SetTimer(StaminaTimerHandle, this, &ATPSCharacter::UpdateStamina, 0.1f, true);

	GetWorldTimerManager().SetTimer(SprintTimerHandle, this, &ATPSCharacter::UpdateSprint, 0.1f, true);

	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		if (CursorMaterial && (GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority))
		{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}
}

void ATPSCharacter::SetupPlayerInputComponent(UInputComponent* InputComp)
{
	Super::SetupPlayerInputComponent(InputComp);

	InputComp->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);
	InputComp->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);

	InputComp->BindAction(TEXT("Roll"), EInputEvent::IE_Pressed, this, &ATPSCharacter::StartRoll_OnServer);
	
	InputComp->BindAction(TEXT("MoveModeSprint"), EInputEvent::IE_Pressed, this, &ATPSCharacter::OnStartSprint);
	InputComp->BindAction(TEXT("MoveModeSprint"), EInputEvent::IE_Released, this, &ATPSCharacter::OnStopSprint);

	InputComp->BindAction(TEXT("AimEvent"), EInputEvent::IE_Pressed, this, &ATPSCharacter::InputAimPressed);
	InputComp->BindAction(TEXT("AimEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::InputAimReleased);

	InputComp->BindAction(TEXT("MoveModeWalk"), EInputEvent::IE_Pressed, this, &ATPSCharacter::InputWalkPressed);
	InputComp->BindAction(TEXT("MoveModeWalk"), EInputEvent::IE_Released, this, &ATPSCharacter::InputWalkReleased);

	InputComp->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATPSCharacter::InputAttackPressed);
	InputComp->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::InputAttackReleased);

	InputComp->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::TryReloadWeapon);

	InputComp->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATPSCharacter::TrySwitchNextWeapon);
	InputComp->BindAction(TEXT("SwitchPreviousWeapon"), EInputEvent::IE_Pressed, this, &ATPSCharacter::TrySwitchPreviousWeapon);

	InputComp->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ATPSCharacter::TryAbilityEnabled);

	InputComp->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &ATPSCharacter::DropCurrentWeapon);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	InputComp->BindKey(HotKeys[1], IE_Pressed, this, &ATPSCharacter::TKeyPressed<1>);
	InputComp->BindKey(HotKeys[2], IE_Pressed, this, &ATPSCharacter::TKeyPressed<2>);
	InputComp->BindKey(HotKeys[3], IE_Pressed, this, &ATPSCharacter::TKeyPressed<3>);
	InputComp->BindKey(HotKeys[4], IE_Pressed, this, &ATPSCharacter::TKeyPressed<4>);
	InputComp->BindKey(HotKeys[5], IE_Pressed, this, &ATPSCharacter::TKeyPressed<5>);
	InputComp->BindKey(HotKeys[6], IE_Pressed, this, &ATPSCharacter::TKeyPressed<6>);
	InputComp->BindKey(HotKeys[7], IE_Pressed, this, &ATPSCharacter::TKeyPressed<7>);
	InputComp->BindKey(HotKeys[8], IE_Pressed, this, &ATPSCharacter::TKeyPressed<8>);
	InputComp->BindKey(HotKeys[9], IE_Pressed, this, &ATPSCharacter::TKeyPressed<9>);
	InputComp->BindKey(HotKeys[0], IE_Pressed, this, &ATPSCharacter::TKeyPressed<0>);
}

void ATPSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATPSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATPSCharacter::InputAttackPressed()
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive() && !bIsRolling)
	{
		AttackCharEvent(true);
	}
}

void ATPSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATPSCharacter::InputWalkPressed()
{
	WalkEnabled = true;
	ChangeMovementState();
}

void ATPSCharacter::InputWalkReleased()
{
	WalkEnabled = false;
	ChangeMovementState();
}

void ATPSCharacter::InputAimPressed()
{
	AimEnabled = true;
	ChangeMovementState();
}

void ATPSCharacter::InputAimReleased()
{
	AimEnabled = false;
	ChangeMovementState();
}


void ATPSCharacter::OnStartSprint()
{
	bIsShiftPressed = true;
	UpdateSprint();
}

void ATPSCharacter::OnStopSprint()
{
	bIsShiftPressed = false;
	UpdateSprint();
}


void ATPSCharacter::MovementTick(float DeltaTime)
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive())
	{
		if (GetController() && GetController()->IsLocalPlayerController())
		{

			AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
			AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

			FString SEnum = UEnum::GetValueAsString(GetMovementState());
			UE_LOG(LogTPS_Net, Warning, TEXT("Movement state - %s"), *SEnum);

			if (MovementState == EMovementState::Sprint_State)
			{
				FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
				FRotator myRotator = myRotationVector.ToOrientationRotator();
				
				SetActorRotation((FQuat(myRotator)));
				SetActorRotationYaw_OnServer(myRotator.Yaw);
			}
			else
			{
				APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
				if (MyController && !bIsRolling)
				{
					FHitResult ResultHit;
					// MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
					MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

					float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
					SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
					SetActorRotationYaw_OnServer(FindRotatorResultYaw);

					if (CurrentWeapon)
					{
						FVector Displacement = FVector(0);
						bool bIsReduceDispersion = false;
						switch (MovementState)
						{
						case EMovementState::Aim_State:
							Displacement = FVector(0.0f, 0.0f, 160.0f);
							//CurrentWeapon->ShouldReduceDispersion = true;
							bIsReduceDispersion = true;
							break;
						case EMovementState::AimWalk_State:
							//CurrentWeapon->ShouldReduceDispersion = true;
							Displacement = FVector(0.0f, 0.0f, 160.0f);
							bIsReduceDispersion = true;
							break;
						case EMovementState::Walk_state:
							Displacement = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouldReduceDispersion = false;
							break;
						case EMovementState::Run_state:
							Displacement = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouldReduceDispersion = false;
							break;
						case EMovementState::Sprint_State:
							break;
						default:
							break;
						}

						//CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
						CurrentWeapon->UpdateWeaponByCharMovementState_OnServer(ResultHit.Location + Displacement, bIsReduceDispersion);
					}
				}
			}
		}
	}
}

void ATPSCharacter::CharacterUpdate()
{
	float TargetSpeed = 600.f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		TargetSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::AimWalk_State:
		TargetSpeed = MovementInfo.AimWalkSpeed;
		break;
	case EMovementState::Walk_state:
		TargetSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_state:
		TargetSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		TargetSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = TargetSpeed;
}

void ATPSCharacter::ChangeMovementState()
{
	EMovementState NewState = EMovementState::Run_state;

	if (!AimEnabled && !WalkEnabled && !SprintEnabled)
	{
		NewState = EMovementState::Run_state;
	}
	else 
	{
		if (SprintEnabled && bIsSprintAngleOk && CurrentStamina > 5)
		{
			AimEnabled = false;
			WalkEnabled = false;
			NewState = EMovementState::Sprint_State;
		}
		else
		{
			if (WalkEnabled && !SprintEnabled && AimEnabled)
			{
				NewState = EMovementState::AimWalk_State;
			}
			else
			{
				if (WalkEnabled && !SprintEnabled && !AimEnabled)
				{
					NewState = EMovementState::Walk_state;
				}
				else
				{
					if (!WalkEnabled && !SprintEnabled && AimEnabled)
					{
						NewState = EMovementState::Aim_State;
					}
				}
			}
		}
	}
	
	SetMovementState_OnServer(NewState);
	
	//CharacterUpdate();

	//weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon_OnServer(NewState);
	}
}

void ATPSCharacter::UpdateSprint()
{
	FVector	SprintDirection = GetActorForwardVector().GetSafeNormal();
	FVector CurrentVelocity = GetCharacterMovement()->Velocity.GetSafeNormal();
	float DotProduct = FVector::DotProduct(SprintDirection, CurrentVelocity);
	float Angle = FMath::Acos(DotProduct) * (180.0f / PI);

	bool bShouldSprint = bIsShiftPressed && Angle <= MaxSprintAngle && CurrentStamina > 0;

	if (bShouldSprint != SprintEnabled)
	{
		SprintEnabled = bShouldSprint;
		ChangeMovementState();
	}

	bIsSprintAngleOk = (Angle <= MaxSprintAngle);
}

void ATPSCharacter::UpdateStamina()
{
	const float DeltaTime = 0.1f;
	float PhysicalSpeed = GetCharacterMovement()->Velocity.Size();

	if (MovementState == EMovementState::Sprint_State && PhysicalSpeed > 0)
	{
		CurrentStamina -= StaminaDecreaseRate * DeltaTime;
		if (CurrentStamina <= 0)
		{
			CurrentStamina = 0;
			SprintEnabled = false;
			ChangeMovementState();
		}
	}
	else if (MovementState == EMovementState::Run_state && PhysicalSpeed > 0)
	{
		CurrentStamina += RunStaminaIncreaseRate * DeltaTime;

		if (CurrentStamina >= MaxStamina)
		{
			CurrentStamina = MaxStamina;
		}
	}
	else 
	{
		CurrentStamina += StaminaIncreaseRate * DeltaTime;
			if (CurrentStamina > MaxStamina)
			{
				CurrentStamina = MaxStamina;
			}
	}

	ChangeCurrentStaminaEvent_Multicast(CurrentStamina);
}

void ATPSCharacter::ChangeCurrentStaminaEvent_Multicast_Implementation(float Stamina)
{
	OnChangeCurrentStamina.Broadcast(Stamina);
}

void ATPSCharacter::ChangeRollCooldownEvent_Multicast_Implementation(bool RollCooldown)
{
	OnChangeRollCooldown.Broadcast(RollCooldown);
}

//roll input
void ATPSCharacter::StartRoll_OnServer_Implementation()
{
	if (!bIsRolling && !bIsRollCoolDown && CurrentStamina > 20.0f && InventoryComponent->CurrentLevel >= 2 && !CurrentWeapon->WeaponReloading)
	{
		bIsRolling = true;

		AttackCharEvent(false);
		GetCharacterMovement()->MaxWalkSpeed = 0.0f;

		RollAnimLength = RollAnim->GetPlayLength();
		RollDistance = RollSpeed * RollAnimLength;

		FVector ForwardVector = GetActorForwardVector();
		FVector StartLocation = GetActorLocation();
		FVector EndLocation = StartLocation + ForwardVector * RollDistance;

		RollStartLocation = StartLocation;
		RollEndLocation = EndLocation;
		RollInterpAlpha = 0.0f;

		GetWorldTimerManager().SetTimer(RollUpdateTimerHandle, this, &ATPSCharacter::UpdateRoll_OnServer, 0.016f, true);
		PlayAnim_Multicast(RollAnim);

		GetWorldTimerManager().SetTimer(RollTimerHandle, this, &ATPSCharacter::EndRoll_OnServer, RollAnimLength, false);

		bIsRollCoolDown = true;
		ChangeRollCooldownEvent_Multicast(bIsRollCoolDown);
		GetWorldTimerManager().SetTimer(RollCoolDownTimerHandle, this, &ATPSCharacter::ResetRollCoolDown, RollCoolDownTime, false);

		CurrentStamina -= 20.0f;
		ChangeCurrentStaminaEvent_Multicast(CurrentStamina);
	}
}

void ATPSCharacter::UpdateRoll_OnServer_Implementation()
{
	if (bIsRolling)
	{
		RollInterpAlpha += 0.016f / RollAnimLength;

		FVector NewLocation = FMath::Lerp(RollStartLocation, RollEndLocation, RollInterpAlpha); // ���������� ������� �������� � ����������� �� alpha(ex. a = 0, b = 100, alpha = 0.5 return 50)

		//SetActorLocation(NewLocation, true);
		UpdateRoll_Multicast(NewLocation);

		if (RollInterpAlpha >= 1.0f)
		{
			EndRoll_OnServer();
		}
	}	
}

void ATPSCharacter::UpdateRoll_Multicast_Implementation(FVector NewLocation)
{
	SetActorLocation(NewLocation, true);
}

void ATPSCharacter::EndRoll_OnServer_Implementation()
{
	bIsRolling = false;

	GetCharacterMovement()->MaxWalkSpeed = 600.f;

	GetWorldTimerManager().ClearTimer(RollUpdateTimerHandle);
}

void ATPSCharacter::ResetRollCoolDown()
{
	bIsRollCoolDown = false;
	ChangeRollCooldownEvent_Multicast(bIsRollCoolDown);
}

void ATPSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo check melee or range
		myWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void ATPSCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	//on server
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->IdWeaponName = IdWeaponName;
					myWeapon->WeaponSettings = myWeaponInfo;

					//myWeapon->AdditionalWeaponInfo.Ammo = myWeaponInfo.MaxAmmo;
					
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon_OnServer(MovementState);

					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					
					CurrentIndexWeapon = NewCurrentIndexWeapon;

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATPSCharacter::WeaponFireStart);

					if (CurrentWeapon->GetWeaponAmmo() <= 0 && CurrentWeapon->CheckCanWeaponReload())
					{
						CurrentWeapon->InitReload();
					}

					if (InventoryComponent)
					{
						InventoryComponent->OnWeaponAmmoAvaible.Broadcast(myWeapon->WeaponSettings.WeaponType);
					}
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}
void ATPSCharacter::TryReloadWeapon()
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive() && CurrentWeapon && !CurrentWeapon->WeaponReloading && !bIsRolling)
	{
		TryReloadWeapon_OnServer();
	}
}

void ATPSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}
void ATPSCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSettings.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void ATPSCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponFireStart_BP(Anim);
}

void ATPSCharacter::TrySwitchWeaponToIndexByKeyInput_OnServer_Implementation(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex) && !bIsRolling)
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->WeaponReloading)
				{
					CurrentWeapon->CancelReload();
				}
					
			}

			bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
}

void ATPSCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		InventoryComponent->DropWeaponByIndex_OnServer(CurrentIndexWeapon);
	}
}

void ATPSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//bp
}

void ATPSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	//bp
}

void ATPSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	//bp
}

UDecalComponent* ATPSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

AWeaponDefault* ATPSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

EMovementState ATPSCharacter::GetMovementState()
{
	return MovementState;
}

TArray<UTPS_StateEffect*> ATPSCharacter::GetCurrentEffectsOnChar()
{
	return Effects;
}

int32 ATPSCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}

bool ATPSCharacter::GetIsAlive()
{
	bool result = false;
	if (CharHealthComponent)
	{
		result = CharHealthComponent->GetIsAlive();
	}
	return result;
}

void ATPSCharacter::TrySwitchNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//have more than 1 weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryComponent)
		{
			if(InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{ }
		}
	}
}

void ATPSCharacter::TrySwitchPreviousWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{ }
		}
	}
}

void ATPSCharacter::TryAbilityEnabled() // to do cd
{
	if (AbilityEffect)
	{
		UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}

}

EPhysicalSurface ATPSCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (CharHealthComponent)
	{
		if (CharHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return Result;
}

TArray<UTPS_StateEffect*> ATPSCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATPSCharacter::RemoveEffect_Implementation(UTPS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);

	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		EffectRemove = RemoveEffect;
		SwitchEffect(RemoveEffect, false);
	}
}

void ATPSCharacter::AddEffect_Implementation(UTPS_StateEffect* newEffect)
{
	Effects.Add(newEffect);

	if (!newEffect->bIsAutoDestroyParticleEffect)
	{
		EffectAdd = newEffect;
		SwitchEffect(EffectAdd, true);
	}
	else
	{
		if (newEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(newEffect->ParticleEffect);
		}
	}
}

void ATPSCharacter::TryReloadWeapon_OnServer_Implementation()
{
	if (CurrentWeapon->GetWeaponAmmo() < CurrentWeapon->WeaponSettings.MaxAmmo && CurrentWeapon->CheckCanWeaponReload())
	{
		CurrentWeapon->InitReload();
	}
}

void ATPSCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
}

void ATPSCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}

void ATPSCharacter::SetActorRotationYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationYaw_Multicast(Yaw);
}

void ATPSCharacter::SetActorRotationYaw_Multicast_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ATPSCharacter::CharDead_BP_Implementation()
{
	//bp
}

void ATPSCharacter::CharDead()
{
	CharDead_BP();
	if (HasAuthority())
	{
		float TimeAnim = 0.0f;
		int32 rnd = FMath::RandHelper(DeadsAnim.Num());
		if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
		{
			TimeAnim = DeadsAnim[rnd]->GetPlayLength();
			//GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
			PlayAnim_Multicast(DeadsAnim[rnd]);
		}

		if (GetController())
		{
			GetController()->UnPossess();
		}

		//timer ragdoll
		GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATPSCharacter::EnableRagdoll_Multicast, TimeAnim, false);

		SetLifeSpan(20.0f);

		if (GetCurrentWeapon())
		{
			GetCurrentWeapon()->SetLifeSpan(20.0f);
		}
	}
	else
	{
		if (GetCursorToWorld())
		{
			GetCursorToWorld()->SetVisibility(false);
		}

		AttackCharEvent(false);

	}

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	}
}

void ATPSCharacter::EnableRagdoll_Multicast_Implementation()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float ATPSCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (CharHealthComponent && CharHealthComponent->GetIsAlive())
	{
		CharHealthComponent->ChangeHealthValue_OnServer(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			FHitResult HitResult = myProjectile->GetLastHitResult();
			LastHitBoneName = HitResult.BoneName;
			UTypes::AddEffectBySurfaceType(this, LastHitBoneName, myProjectile->ProjectileSettings.Effect, GetSurfaceType());
		}
	}

	return ActualDamage;
}

void ATPSCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* Anim)
{
	if (GetMesh() && GetMesh()->GetAnimInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(Anim);
	}
}

void ATPSCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void ATPSCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATPSCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATPSCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName("Spine_01"));
}

void ATPSCharacter::SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName NameBoneToAttached = Effect->NameBone;
			FVector Loc = FVector(0);

			USkeletalMeshComponent* myMesh = GetMesh();
			if (myMesh)
			{
				UParticleSystemComponent* newParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(newParticleSystem);
			}
		}
	}
	else
	{
		if (Effect && Effect->ParticleEffect)
		{
			int32 i = 0;
			bool bIsFind = false;
			if (ParticleSystemEffects.Num() > 0)
			{
				while (i < ParticleSystemEffects.Num() && !bIsFind)
				{
					if (ParticleSystemEffects[i] && ParticleSystemEffects[i]->Template && Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
					{
						bIsFind = true;
						ParticleSystemEffects[i]->DeactivateSystem();
						ParticleSystemEffects[i]->DestroyComponent();
						ParticleSystemEffects.RemoveAt(i);
					}
					i++;
				}
			}
		}
	}
//if (Effect)
//{
//	FName NameBoneToAttached = NameBoneHit;
//	FVector Loc = FVector(0);

//	USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
//	if (myMesh)
//	{
//		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
//	}
//	else
//	{
//		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
//	}
//}
}

bool ATPSCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

void ATPSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPSCharacter, MovementState);
	DOREPLIFETIME(ATPSCharacter, CurrentWeapon);
	DOREPLIFETIME(ATPSCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(ATPSCharacter, Effects);
	DOREPLIFETIME(ATPSCharacter, EffectAdd);
	DOREPLIFETIME(ATPSCharacter, EffectRemove);
	DOREPLIFETIME(ATPSCharacter, bIsRolling);
	DOREPLIFETIME(ATPSCharacter, bIsRollCoolDown);
	DOREPLIFETIME(ATPSCharacter, RollStartLocation);
	DOREPLIFETIME(ATPSCharacter, RollEndLocation);
	DOREPLIFETIME(ATPSCharacter, CurrentStamina);
}