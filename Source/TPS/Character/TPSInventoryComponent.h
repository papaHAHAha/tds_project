// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPSInventoryComponent.generated.h"



DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FAdditionalWeaponInfo, WeaponAdditionalInfo, int32, NewCurrentIndexWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, TypeAmmo, int32, Cout);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexSlot, FAdditionalWeaponInfo, AdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoEmpty, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAvaible, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlots, int32, IndexSlotChange, FWeaponSlot, NewInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponNotHaveRound, int32, IndexSlotWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveRound, int32, IndexSlotWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeCurrentExp, int32, CurrentExp);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeCurrentLevel, int32, CurrentLevel);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeExpNeedToNextLevel, int32, ExpNeedToNextLevel);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UTPSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTPSInventoryComponent();

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnSwitchWeapon OnSwitchWeapon;

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnAmmoChange OnAmmoChange;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponAmmoAvaible OnWeaponAmmoAvaible;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnUpdateWeaponSlots OnUpdateWeaponSlots;

	//Event current weapon not have additional_Rounds 
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponNotHaveRound OnWeaponNotHaveRound;
	//Event current weapon have additional_Rounds 
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponHaveRound OnWeaponHaveRound;

	UPROPERTY(BlueprintAssignable, Category = "Level System")
	FOnChangeCurrentExp OnChangeCurrentExp;

	UPROPERTY(BlueprintAssignable, Category = "Level System")
	FOnChangeCurrentLevel OnChangeCurrentLevel;

	UPROPERTY(BlueprintAssignable, Category = "Level System")
	FOnChangeExpNeedToNextLevel OnChangeExpNeedToNextLevel;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "Level System")
	int32 CurrentExp = 0;

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "Level System")
	int32 ExpNeedToNextLevel = 100;

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "Level System")
	int32 CurrentLevel = 1;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Weapons")
	TArray<FWeaponSlot> WeaponSlots;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Weapons")
	TArray<FAmmoSlot> AmmoSlots;

	int32 MaxSlotsWeapon = 0;

	UFUNCTION(NetMulticast, Reliable)
	void AddExperience_Multicast(int32 Amount);

	bool SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward);

	bool SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviousIndex, FAdditionalWeaponInfo PreviousWeaponInfo);

	void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo);

	FAdditionalWeaponInfo GetAdditionalInfoWeapon(int32 IndexWeapon);

	int32 GetWeaponIndexSlotByName(FName IdWeaponName);

	FName GetWeaponNameBySlotIndex(int32 IndexSlot);

	bool GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType& WeaponType);

	bool GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType& WeaponType);

	UFUNCTION(BlueprintCallable)
	void AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo);

	bool CheckAmmoForWeapon(EWeaponType TypeWeapon, int8& AvaibleAmmoForWeapon);

	//interface pickup
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckCanTakeAmmo(EWeaponType AmmoType);

	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckCanTakeWeapon(int32& FreeSlot);

	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo);

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void TryGetWeaponToInventory_OnServer(AActor* PickUpActor, FWeaponSlot NewWeapon);

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void DropWeaponByIndex_OnServer(int32 ByIndex);

	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem& DropItemInfo);

	//

	UFUNCTION(BlueprintCallable, Category = "Inv")
	TArray<FWeaponSlot> GetWeaponSlots();

	UFUNCTION(BlueprintCallable, Category = "Inv")
	TArray<FAmmoSlot> GetAmmoSlots();

	UFUNCTION(NetMulticast, Reliable)
	void ChangeCurrentExpEvent_Multicast(int32 NewExp);

	UFUNCTION(NetMulticast, Reliable)
	void ChangeCurrentLevelEvent_Multicast(int32 NewLevel);

	UFUNCTION(NetMulticast, Reliable)
	void ChangeExpNeedToNextLevelEvent_Multicast(int32 NewExpNeedToNextLevel);

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "LevelSystem")
	void AddExperience_OnServer(int32 Amount);

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inv")
	void InitInventory_OnServer(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo);

	UFUNCTION(NetMulticast, Reliable)
	void AmmoChangeEvent_Multicast(EWeaponType TypeWeapon, int32 Cout);

	UFUNCTION(Server, Reliable)
	void SwitchWeaponEvent_OnServer(FName WeaponIdName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);

	UFUNCTION(NetMulticast, Reliable)
	void WeaponAdditionalInfoChangeEvent_Multicast(int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo);

	UFUNCTION(NetMulticast, Reliable)
	void WeaponAmmoEmptyEvent_Multicast(EWeaponType TypeWeapon);

	UFUNCTION(NetMulticast, Reliable)
	void WeaponAmmoAvaibleEvent_Multicast(EWeaponType TypeWeapon);

	UFUNCTION(NetMulticast, Reliable)
	void UpdateWeaponSlotsEvent_Multicast(int32 IndexSlotChange, FWeaponSlot NewInfo);

	UFUNCTION(NetMulticast, Reliable)
	void WeaponNotHaveRoundEvent_Multicast(int32 IndexSlotWeapon);

	UFUNCTION(NetMulticast, Reliable)
	void WeaponHaveRoundEvent_Multicast(int32 IndexSlotWeapon);

};
