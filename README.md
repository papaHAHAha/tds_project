# TPS

Developed with Unreal Engine 4 Top Down Shooter

***Key features***
- **diverse arsenal of weapons:** *unique weapons and ammunition*
- **variety of enemies:** *their behavior and bosses*
- **Movement and dodging mechanics:** *responsive controls with the possibility of rolls* 
- **Character development system:** *experience system to improve your character*
- **Multiplayer:** *cooperative play*
- **Artificial Intelligence System:** *intelligent enemies*
- **Sound and visual effects:** *atmosphere creation*